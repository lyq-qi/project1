import os
from flask import Flask, render_template, request, send_from_directory
from backend.create_db import app, db, Book, create_books
import json

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def index(path):
  return render_template("index.html")

@app.route("/api/test")
def test():
      return "TEST123456789"

@app.route("/api")
def test1():
      return "Welcome to this web API!"

@app.route('/api/books')
def book():
	book_list = db.session.query(Book).all()
	print(book_list)
	return str([book.title for book in book_list])

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80, threaded=True, debug=True)
