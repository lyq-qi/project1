#!/usr/bin/env python3

# ---------------------------
# projects/IDB3/models.py
# Fares Fraij
# ---------------------------

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os
from flask_cors import CORS



app = Flask(__name__, static_folder="../frontend/build/static", template_folder="../frontend/build")
CORS(app)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:1234@104.154.194.177:5432/postgres')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
db = SQLAlchemy(app)

# ------------
# Book
# ------------
class Book(db.Model):
    """
    Book class has two attrbiutes
    title
    id
    """
    __tablename__ = 'book'

    title = db.Column(db.String(80), nullable = False)
    id = db.Column(db.Integer, primary_key = True)

db.drop_all()
db.create_all()
